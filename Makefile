.POSIX:
.SUFFIXES:

# Build Variables
BIN = usericon

PKGFLAGS = `pkg-config --cflags --libs\
		   gobject-2.0\
		   glib-2.0\
		   accountsservice`

SRC = usericon.c
OBJ = $(SRC:.c=.o)

# Install Variables
PREFIX   = /usr/local
BINDIR   = bin
MANDIR   = share/man/man1
DOCDIR 	 = share/doc/usericon
BINPERMS = 755
DOCPERMS = 644

$(BIN): $(OBJ)
	$(CC) $(PKGFLAGS) $(CFLAGS) -o $@ $(OBJ)

.SUFFIXES: .c .o
.c.o:
	$(CC) $(PKGFLAGS) $(CFLAGS) -c $<

install: $(BIN)
	strip $(BIN)
	install -d $(DESTDIR)$(PREFIX)/$(BINDIR)
	install -m $(BINPERMS) $(BIN) $(DESDIR)$(PREFIX)/$(BINDIR)
	install -d $(DESTDIR)$(PREFIX)/$(DOCDIR)
	install -m $(DOCPERMS) COPYING $(DESTDIR)$(PREFIX)/$(DOCDIR)
	install -m $(DOCPERMS) README.md $(DESTDIR)$(PREFIX)/$(DOCDIR)
	install -d $(DESTDIR)$(PREFIX)/$(MANDIR)
	install -m $(DOCPERMS) usericon.1 $(DESTDIR)/$(PREFIX)/$(MANDIR)
	gzip -f $(DESTDIR)$(PREFIX)/$(MANDIR)/usericon.1

clean:
	rm -f $(BIN) $(OBJ)
