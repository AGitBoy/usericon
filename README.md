# usericon
usericon is a simple way to set your user's avatar via the command line on GNU/Linux or the BSDs. It works similarly to the way modern GUI desktops set it.

## Why
Due to GNOME's development team's decision to abandon .face files in favor of the more 
programmatic AccountService, I wrote this program, so users of minimalistic window managers,
such as i3wm and openbox, can set their user icon easily.

# Install
You need:
* accountsservice development packages
* gobject development packages
* A working C compiler
* GNU Make

Simply run:
```
make && sudo make install
```
in the source directory

## Usage
Simply:
```
usericon PATH/TO/ICON
```

or, if you want to set it for another user:
```
usericon -u USERNAME PATH/TO/ICON
```

## License
This project is available under the GNU GPLv3 or later.
