/*
 * Copyright (C) 2022 Avalon Williams
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <limits.h>
#include <glib.h>
#include <glib/gprintf.h>
#include <act/act.h>
#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <pwd.h>
#include <getopt.h>
#include <errno.h>

static GMainLoop *loop;
static gchar *username;

static struct option lopts[] = {
     { "user",    required_argument, 0, 'u' },
     { "help",    no_argument,       0,  0  },
     { "version", no_argument,       0,  0  },
     { 0,         0,                 0,  0  }
};

// String printed out with --help
static const gchar *usagestr =
    "Usage: usericon [OPTIONS] FILE\n"
    "Options:\n"
    "  -u, --user   \tChange icon for another user. This may require root privileges.\n"
    "      --help   \tPrints this help text\n"
    "      --version\tPrints the version information\n";

// String printed out with --version
static const gchar *versionstr =
    "usericon 1.0\n"
    "Copyright (C) 2019 Aidan Williams\n"
    "License GPLv3+: GNU GPL version 3 or later <https://gnu.org/licenses/gpl.html>\n"
    "This is free software: you are free to change and redistribute it.\n"
    "There is NO WARRANTY, to the extent permitted by law.\n\n"
    "Written by Aidan Williams";

// Prints usage
void usage(gchar *progpth, int err)
{
    fputs(usagestr, err ? stderr : stdout);
    exit(err);
}

// Prints version
void version()
{
    puts(versionstr);
    exit(EXIT_SUCCESS);
}

void user_callback(ActUser *user, GParamSpec *pspec, gchar *icon)
{
    act_user_set_icon_file(user, icon);
    g_main_loop_quit(loop);
}

void set_user_icon(ActUserManager *manager, GParamSpec *pspec, gchar *icon)
{
    ActUser *user = act_user_manager_get_user(manager,
                                              username ? username : g_get_user_name());
    gboolean loaded; 

    g_object_get(user, "is-loaded", &loaded, NULL);

    if (loaded)
        act_user_set_icon_file(user, icon);
    else
        g_signal_connect(user, "notify::is-loaded",
                         G_CALLBACK(user_callback), icon);
}

int main(int argc, gchar **argv)
{
    int opt, opti;
    char *arg;
    gchar *iconpath;

    while ((opt = getopt_long(argc, argv, "-u:", lopts, &opti)) != -1) {
        switch (opt) {
        case 0:
            arg = (char *)lopts[opti].name;

            if (strcmp(arg, "help") == 0)
                usage(argv[0], 0);
            else
                version();
        case 1:
            iconpath = optarg;
            break;
        case 'u':
            username = optarg;
            break;
        default:
            usage(argv[0], 1);
        }
    }

    if (!iconpath)
        usage(argv[0], 1);

    if (g_file_test(iconpath, G_FILE_TEST_IS_DIR)) {
        fprintf(stderr, "usericon: %s: Is a directory\n", iconpath);
        return 1;
    }    
    
    gchar *pth = realpath(iconpath, NULL);

    if (!pth) {
        fprintf(stderr, "%s: %s\n", iconpath, strerror(errno));
        exit(errno);
    }
    
    loop = g_main_loop_new(NULL, false);

    ActUserManager *manager = act_user_manager_get_default();
    gboolean loaded;

    g_object_get(manager, "is-loaded", &loaded, NULL);

    if (loaded)
        set_user_icon(manager, NULL, pth);
    else
        g_signal_connect(manager, "notify::is-loaded",
                         G_CALLBACK(set_user_icon), pth);

    g_main_loop_run(loop);

    return 0;
}
